FROM alpine


RUN \
    apk --no-cache add \
    alsa-utils \
    alsa-utils-doc \
    alsa-lib \
    alsaconf
